package com.example.lab2bai2;

import android.app.Presentation;
import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLEncoder;

public class BackgroundTask_POST extends AsyncTask <Void, Void, Void>{

    String link = MainActivity.SERVER_NAME;
    Context context;
    String strDai, strRong;
    TextView tvResult;
    ProgressDialog pDialog;
    String strResult;

    public BackgroundTask_POST(Context context, String strDai, String strRong, TextView tvResult){

        this.context = context;
        this.strDai = strDai;
        this.strRong = strRong;
        this.tvResult = tvResult;

    }
    @Override
    protected Void doInBackground(Void... voids) {
        try{
            URL url = new URL(link);
            String param = "chieurong=" + URLEncoder.encode(strRong,"utf-8") +
                    "chieudai=" + URLEncoder.encode(strDai,"utf-8");
            HttpURLConnection urlConnection = (HttpURLConnection)url.openConnection();
            urlConnection.setDoOutput(true);
            urlConnection.setRequestMethod("POST");
            urlConnection.setFixedLengthStreamingMode(param.getBytes().length);
            urlConnection.setRequestProperty("Content-Type", "application/x-www-forum-urlencodeed");

            PrintWriter print = new PrintWriter(urlConnection.getOutputStream());
            print.print(param);
            print.close();

            String line ="";
            BufferedReader bfr = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            StringBuffer sb = new StringBuffer();
            while ((line=bfr.readLine())!=null){
                sb.append(line);
            }
            strResult = sb.toString();
            urlConnection.disconnect();

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } ;



        return null;
    }

    protected void onPreExecute(){
        super.onPreExecute();
        pDialog = new ProgressDialog(context);
        pDialog.setMessage("Cal....");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

    }

    protected  void onPostExecute (Void result){
        super.onPostExecute(result);
        if(pDialog.isShowing()){
            pDialog.dismiss();
        }
        tvResult.setText(strResult);
    }


}
