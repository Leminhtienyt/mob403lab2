package com.example.lab2bai2;

import androidx.appcompat.app.AppCompatActivity;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;

import android.Manifest;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Switch;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    public static final String SERVER_NAME = "http://localhost/tienlmps06128/rectangle_POST.php";

    private EditText edtRong, edtDai;
    private Button btnSend;
    private TextView tvResult;
    String strDai, strRong;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        edtDai = (EditText) findViewById(R.id.edtDai);
        edtRong = (EditText) findViewById(R.id.edtRong);
        btnSend = (Button) findViewById(R.id.btnSend);
        tvResult = (TextView) findViewById(R.id.tvResult);
        btnSend.setOnClickListener(this);


    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.btnSend:
                strDai = edtDai.getText().toString();
                strRong = edtRong.getText().toString();
                BackgroundTask_POST backgroundTask_post = new BackgroundTask_POST(this, strRong, strDai, tvResult);
                backgroundTask_post.execute();
                break;
        }

    }



}
