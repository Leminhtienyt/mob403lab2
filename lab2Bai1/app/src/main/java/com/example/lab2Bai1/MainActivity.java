package com.example.lab2Bai1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.style.BackgroundColorSpan;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    public static final String SEVER_NAME = "http://localhost/tienlmps06128/student_GET.php";

    private EditText edtname,edtSorce;
    private Button btnSend;
    private TextView tvRedult;
    String strName, strScore;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edtname = (EditText) findViewById(R.id.editName);
        edtSorce = (EditText) findViewById(R.id.editSorce);

        btnSend = (Button) findViewById(R.id.btnSend);
        tvRedult = (TextView) findViewById(R.id.textView);
        btnSend.setOnClickListener(this);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnSend:
            strName = edtname.getText().toString();
            strScore = edtSorce.getText().toString();
            BackgroundTask_GET backgroundTask = new BackgroundTask_GET(this,tvRedult,strName,strScore);
            break;
        }
    }
}
