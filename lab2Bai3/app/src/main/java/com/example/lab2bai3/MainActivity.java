package com.example.lab2bai3;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    public static final String SERVER_NAME = "C:\\xampp\\htdocs\\tienlmps06128/canh_POST.php";
    private EditText edtCanh;
    private Button btnSend;
    private TextView tvKetQua;
    String strCanh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        edtCanh = (EditText) findViewById(R.id.edtCanh);
        btnSend = (Button) findViewById(R.id.btnSend);
        tvKetQua = (TextView) findViewById(R.id.tvKetQua);
        btnSend.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        strCanh = edtCanh.getText().toString();
        BackgroundTask_POST backgroundTask_post = new BackgroundTask_POST(this, tvKetQua);
        backgroundTask_post.execute();
    }
}