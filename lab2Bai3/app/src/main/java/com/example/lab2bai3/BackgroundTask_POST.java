package com.example.lab2bai3;

import android.app.ProgressDialog;
import android.content.Context;
import android.os.AsyncTask;
import android.view.View;
import android.widget.TextView;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ProtocolException;
import java.net.URL;
import java.net.URLEncoder;

public class BackgroundTask_POST extends AsyncTask<String, Void, Void> {

    String link = MainActivity.SERVER_NAME;
    Context context;
    TextView tvKetQua;
    ProgressDialog pDialog;
    String strKetQua;

    public BackgroundTask_POST(Context context, TextView tvKetQua) {
        this.context = context;
        this.tvKetQua = tvKetQua;
    }

    @Override
    protected Void doInBackground(String... pramas) {
        try{
            URL url = new URL(link);
            String param = "canh=" + URLEncoder.encode(pramas[0].toString(),"utf-8");
            HttpURLConnection urlConnection = (HttpURLConnection)url.openConnection();
            urlConnection.setDoOutput(true);
            urlConnection.setRequestMethod("POST");
            urlConnection.setFixedLengthStreamingMode(param.getBytes().length);
            urlConnection.setRequestProperty("Content-Type", "application/x-www-forum-urlencodeed");

            PrintWriter print = new PrintWriter(urlConnection.getOutputStream());
            print.print(param);
            print.close();

            String line ="";
            BufferedReader bfr = new BufferedReader(new InputStreamReader(urlConnection.getInputStream()));
            StringBuffer sb = new StringBuffer();
            while ((line=bfr.readLine())!=null){
                sb.append(line);
            }
            strKetQua = sb.toString();
            urlConnection.disconnect();

        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        } catch (ProtocolException e) {
            e.printStackTrace();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }


        return null;
    }

    protected void onPreExecute(){
        super.onPreExecute();
        pDialog = new ProgressDialog(context);
        pDialog.setMessage("Cal....");
        pDialog.setIndeterminate(false);
        pDialog.setCancelable(false);
        pDialog.show();

    }

    @Override
    protected void onPostExecute (Void result){
        super.onPostExecute(result);
        if (pDialog.isShowing()){
            pDialog.dismiss();
        }
        tvKetQua.setText(strKetQua);

    }

}

